const carList = document.getElementById('app');

// Główna funkcja inicjująca
function init() {
	fetch('assets/data.json')
		.then(response => {
			return response.json();
		})
		.then(data => {
			if (data.answer === '200' && data.offers.length > 0) {

				// Filtrowanie objektów w tablicy i zapisanie ich w zmiennej result
				let result = data.offers.map(item => ({
					photo: item.photo,
					model: item.model,
					engine: item.engine,
					availability: item.availability,
					checkButton: '',
					deleteButton: ''
				}));

				// Tworzenie tabeli
				createTable(result);

				// Dodanie eventów do przycisków
				createEvents();

				console.log('Sukces! Udało się pobrać dane z pliku json.')
			} else {
				console.warn('Błąd! Nie można pobrać danych z pliku json.');
			}
		});
};

// Funkcja generująca tabelkę
function createTable(data) {
	let table = document.createElement('table'),
		thead = document.createElement('thead'),
		tbody = document.createElement('tbody');

	// Literacja po wierszu (to jest tablica z 11 objektami)
	data.forEach((item) => {
		let childrens = '';

		// Literacja po komórce (Object.keys zwraca array)
		Object.keys(item).forEach(index => {
			let elem = item[index];

			// Modyfikacja komórek - kolumna Nazwa
			if (index === 'photo') {
				if (item.photo === undefined) {
					elem = item.model + item.engine;
				} else {
					elem = `<img src="${elem}"/>`;
				}

				// Modyfikacja komórek - kolumna Dostępność
			} else if (index === 'availability') {
				if (item.availability === false) {
					elem = `
						<div class="availability">
							Nie
						</div>
					`;
				} else if (item.availability === true) {
					elem = `
						<div class="availability">
							Tak
						</div>
					`;
				}

				// Modyfikacja komórek - kolumna Zmień
			} else if (index === 'checkButton') {
				if (item.availability === false) {
					elem = `<input class="change-button" type="checkbox">`;
				} else if (item.availability === true) {
					elem = `<input class="change-button" type="checkbox" checked>`;
				}

				// Modyfikacja komórek - kolumna Usuń
			} else if (index === 'deleteButton') {
				elem = `
					<button class="delete-button" type="button">
						<i class="far fa-times-circle"></i>
					</button>
				`;
			}

			childrens += `<td>${elem}</td>`;
		});

		tbody.innerHTML += `
			<tr>
				${childrens}
			</tr>
		`;
	})

	thead.innerHTML = `
		<th>Nazwa</th>
		<th>Model</th>
		<th>Silnik</th>
		<th>Dostępność</th>
		<th>Zmień</th>
		<th>Usuń</th>
	`;

	table.appendChild(thead);
	table.appendChild(tbody);
	carList.appendChild(table);
};


// Funkcja do tworzenia eventów iamnipulowania danymi
function createEvents() {
	let deleteEvents = document.querySelectorAll('.delete-button'),
		changeEvents = document.querySelectorAll('.change-button'),
		trList = document.querySelectorAll('table tr'),
		availabilityList = document.querySelectorAll('div.availability');

	availabilityList.forEach((item, index) => {
		if (item.innerText == 'Nie') {
			trList[index + 1].classList.add('red');
		} else if (item.innerText == 'Tak') {
			trList[index + 1].classList.remove('red');
		}
	});

	deleteEvents.forEach((item, index) => {
		item.addEventListener('click', () => {
			trList[index + 1].setAttribute('style', 'display:none;');
		})
	})

	changeEvents.forEach((item, index) => {
		item.addEventListener('click', () => {
			trList[index + 1].classList.toggle('red');

			if (item.checked == true) {
				availabilityList[index].innerHTML = 'Tak';
			} else {
				availabilityList[index].innerHTML = 'Nie';
			}
		})
	});



};

// Wywołanie głównej funkcji inicjującej
init();